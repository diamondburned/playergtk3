package playerctl

import (
	"errors"
	"strings"

	"github.com/godbus/dbus"
)

// Players is the array
type Players []Player

// Player is the element for each player
type Player struct {
	Player   string
	Metadata Metadata
}

type Metadata struct {
	MprisTrackid     string
	MprisLength      uint64
	MprisArtURL      string
	XesamAlbum       string
	XesamAlbumArtist []string
	XesamArtist      []string
	XesamAutoRating  float64
	XesamDiscNumber  int32
	XesamTitle       string
	XesamTrackNumber int32
	XesamURL         string
	XesamGenre       []string
	XesamUserRating  float32
}

// Update the players
func (pls *Players) Update(conn *dbus.Conn) error {
	*pls = Players{}

	var dbusList []string
	err := conn.BusObject().Call("org.freedesktop.DBus.ListNames", 0).Store(&dbusList)
	if err != nil {
		return err
	}

	for _, v := range dbusList {
		if strings.HasPrefix(v, "org.mpris.MediaPlayer2") {
			player, err := Populate(conn, v)
			if err != nil {
				return err
			}

			*pls = append(*pls, *player)
		}
	}

	return nil
}

// Populate a player
func Populate(conn *dbus.Conn, playerName string) (*Player, error) {
	mpris := conn.Object(playerName, "/org/mpris/MediaPlayer2")
	metadata, err := mpris.GetProperty("org.mpris.MediaPlayer2.Player.Metadata")
	if err != nil {
		return nil, err
	}

	//json, _ := json.MarshalIndent(, "", "\t")

	md := &Metadata{}

	values, ok := metadata.Value().(map[string]dbus.Variant)
	if !ok {
		return nil, errors.New("Metadata not map[string]dbus.Variant")
	}

	for k, variant := range values {
		v := variant.Value()
		if vString, ok := v.(string); ok {
			switch k {
			case "mpris:artUrl":
				md.MprisArtURL = vString
			case "xesam:album":
				md.XesamAlbum = vString
			case "xesam:title":
				md.XesamTitle = vString
			case "xesam:url":
				md.XesamURL = vString
			}
		} else if vObjPath, ok := v.(dbus.ObjectPath); ok {
			md.MprisTrackid = string(vObjPath)
		} else if vStrings, ok := v.([]string); ok {
			for _, vString := range vStrings {
				switch k {
				case "xesam:albumArtist":
					md.XesamAlbumArtist = append(md.XesamAlbumArtist, vString)
				case "xesam:artist":
					md.XesamArtist = append(md.XesamArtist, vString)
				}
			}
		} else if vInt, ok := v.(int32); ok {
			switch k {
			case "xesam:discNumber":
				md.XesamDiscNumber = vInt
			case "xesam:trackNumber":
				md.XesamTrackNumber = vInt
			}
		} else if vUint64, ok := v.(uint64); ok {
			if k == "mpris:length" {
				md.MprisLength = vUint64
			}
		} else if vFloat64, ok := v.(float64); ok {
			if k == "xesam:autoRating" {
				md.XesamAutoRating = vFloat64
			}
		}
	}

	nameArray := strings.Split(playerName, ".")
	pl := &Player{
		Player:   nameArray[len(nameArray)-1],
		Metadata: *md,
	}

	return pl, nil
}
