package playerctl

import (
	"encoding/json"
	"fmt"
	"html"
	"log"
	"os/exec"
	"strings"

	"github.com/gojp/kana"
)

// Status is the state (playing, stopped, etc)
type Status = string

// Signals are signals to control playback
type Signals = string

const (
	/*
	 * Status
	 */

	// Playing is playing
	Playing Status = "Playing"
	// Paused is pausing
	Paused Status = "Paused"
	// Stopped is stopped
	Stopped Status = "Stopped"
	// None means no players found
	None Status = "None"

	/*
	 * Signals
	 */

	// Play shortcut for play
	Play Signals = "play"
	// Pause shortcut for pause
	Pause Signals = "pause"
	// PlayPause shortcut for toggling play and pause
	PlayPause Signals = "play-pause"
	// Stop shortcut for stop
	Stop Signals = "stop"
	// Next shortcut for next
	Next Signals = "next"
	// Previous shortcut for previous
	Previous Signals = "previous"
)

// GetStatus determines if it's playing or not
func GetStatus(player string) Status {
	var cmd *exec.Cmd
	if player != "" {
		cmd = exec.Command("playerctl", "-p", player, "status")
	} else {
		cmd = exec.Command("playerctl", "status")
	}

	stdout, err := cmd.Output()

	if err != nil {
		log.Println(err.Error())
		return None
	}

	switch string(stdout) {
	case "Playing\n":
		return Playing
	case "Paused\n":
		return Paused
	case "Stopped\n":
		return Stopped
	default:
		println(string(stdout))
		return None
	}
}

// RetPlayer returns *Player based on PlayerID
func RetPlayer(input string, players []Player) (Player, error) {
	var player Player
	for _, p := range players {
		sth, _, _ := GiveMeSomething(p)
		if p.Player == input || sth == input {
			player = p
			return player, nil
		}
	}

	return player, fmt.Errorf("no players matched")
}

// GiveMeSomething pretty-prints something (or anything)
// returns: string + Xesam(true|false)
// Todo: expand this function
func GiveMeSomething(p Player) (string, bool, string) {
	if avail(p.Metadata.XesamTitle, p.Metadata.XesamAlbum) && len(p.Metadata.XesamArtist) != 0 {
		info := map[string]string{
			"Artist":  strings.Join(p.Metadata.XesamArtist, ", "),
			"Title":   korewaNihongodesuka(p.Metadata.XesamTitle),
			"Album":   korewaNihongodesuka(p.Metadata.XesamAlbum),
			"TrackID": p.Metadata.MprisTrackid,
		}

		return "<b>Artist:</b> \t\t" + info["Artist"] + "\n" +
			"<b>Title:</b> \t\t" + info["Title"] + "\n" +
			"<b>Album:</b> \t" + info["Album"], true, info["TrackID"]
	}

	return p.Metadata.MprisTrackid, false, p.Metadata.MprisTrackid
}

// GetMetadata is the function to parse GVariant into JSON
func GetMetadata() []Player {
	var playerctl = []Player{}
	// Damn, ahrs
	cmd := exec.Command("bash", "-c", `playerctl -a metadata |sed -e "/<'.*>/s/\"/\\\\\"/g" -e 's|\">}|"}|g' -e "s|^{'|{\"|g" -e "s|<'|\"|g" -e "s|'>|\"|g" -e "s|':|\":|g" -e "s|<\[|[|g" -e "s|\]>|]|g" -e "s|, '|, \"|g" -e "s|\['|[\"|g" -e "s|'\]|\"]|g" -e "s|<boolean ||g" -e "s|>,|,|g" -e "s|<byte ||g" -e "s|>,|,|g" -e "s|<int16 ||g" -e "s|>,|,|g" -e "s|<uint16 ||g" -e "s|>,|,|g" -e "s|<int32 ||g" -e "s|>,|,|g" -e "s|<uint32 ||g" -e "s|>,|,|g" -e "s|<handle ||g" -e "s|>,|,|g" -e "s|<int64 ||g" -e "s|>,|,|g" -e "s|<uint64 ||g" -e "s|>,|,|g" -e "s|<double ||g" -e "s|>,|,|g" -e "s|<string ||g" -e "s|<objectpath ||g" -e "s|<signature ||g" -e "s|>,|,|g" -e "s|>,|,|g" -e "s|>,|,|g" -e "s|<[0-9]+,||g" -e "s|: <|:|g" -e "s|>}$|}|g" -e "s|: '/|: \"/|g" -e 's|\\"}$|"}|g' -e 's|:\\|:|g' -e "s|}{'|},{\"|g" -e 's|"}$|"}]|g' -e 's|^{|[{|g' -e "/[.*]/s/',/\",/g"  -e 's|\\",|",|g' -e '/:\s\[.*\\"\]/s/\\"/"/g' -e "s|\\\\\"}|\"}|g" -e 's|\\\\|\\|g'`)
	stdout, err := cmd.Output()
	if err != nil {
		log.Println(err.Error())
		return playerctl
	}

	players, err := getpls()
	if err != nil {
		log.Println(err.Error())
		return playerctl
	}

	if err := json.Unmarshal(stdout, &playerctl); err != nil {
		log.Println(err.Error())
		return playerctl
	}

	for k := 0; k < len(players)-1; k++ {
		playerctl[k].Player = players[k]
	}

	//log.Println(playerctl)
	return playerctl
}

// CtrlPlayer toggles player actions
func CtrlPlayer(playerID string, action Signals) error {
	log.Println("Received:", playerID, action)
	cmd := exec.Command("playerctl", "--player="+playerID, action)
	if _, err := cmd.Output(); err != nil {
		return err
	}

	return nil
}

func getpls() ([]string, error) {
	cmd := exec.Command("playerctl", "--list-all")
	stdout, err := cmd.Output()

	if err != nil {
		return []string{}, err
	}

	players := strings.Split(string(stdout), "\n")
	return players, nil
}

func avail(str ...string) bool {
	for _, v := range str {
		if v == "" {
			return false
		}
	}

	return true
}

func korewaNihongodesuka(input string) string {
	if kana.IsKana(input) {
		input = kana.KanaToRomaji(input)
	}

	return html.EscapeString(strings.Title(input))
}
