
>>> import dbus

>>> bus = dbus.SessionBus()

>>> dbus_names = bus.get_object( 'org.freedesktop.DBus', '/org/freedesktop/DBus' )

>>> dbus_names
<ProxyObject wrapping <dbus._dbus.SessionBus (session) at 0x7f330ff31678> org.freedesktop.DBus /org/freedesktop/DBus at 0x7f330ff0ad68>

>>> dbus_o = bus.get_object('org.freedesktop.DBus', '/')

>>> dbus_intf = dbus.Interface(dbus_o, 'org.freedesktop.DBus')

>>> dbus_intf.ListNames()
dbus.Array([dbus.String('org.freedesktop.DBus'), dbus.String(':1.7'), dbus.String('org.freedesktop.Notifications'), dbus.String('org.freedesktop.PowerManagement'), dbus.String(':1.8'), dbus.String('org.freedesktop.network-manager-applet'), dbus.String('org.freedesktop.portal.Desktop'), dbus.String(':1.9'), dbus.String(':1.54527'), dbus.String(':1.49819'), dbus.String('org.freedesktop.systemd1'), dbus.String('org.xfce.Xfconf'), dbus.String('com.gexperts.Tilix'), dbus.String('org.fcitx.Fcitx'), dbus.String('org.gtk.vfs.Daemon'), dbus.String('org.pulseaudio.Server'), dbus.String('org.gtk.vfs.mountpoint_20593'), dbus.String(':1.80'), dbus.String('org.freedesktop.impl.portal.desktop.gtk'), dbus.String('org.manjaro.pamac.tray'), dbus.String('org.xfce.Panel'), dbus.String(':1.16690'), dbus.String(':1.60'), dbus.String(':1.61'), dbus.String('org.manjaro.pamac.manager'), dbus.String('org.gnome.GConf'), dbus.String('org.gtk.vfs.UDisks2VolumeMonitor'), dbus.String(':1.63'), dbus.String(':1.41'), dbus.String('org.a11y.Bus'), dbus.String(':1.64'), dbus.String(':1.42'), dbus.String(':1.20'), dbus.String('org.gnome.Identity'), dbus.String(':1.43'), dbus.String(':1.21'), dbus.String('org.gnome.keyring'), dbus.String(':1.88'), dbus.String(':1.66'), dbus.String(':1.44'), dbus.String(':1.22'), dbus.String('org.mpris.MediaPlayer2.spotify'), dbus.String(':1.45'), dbus.String(':1.110'), dbus.String(':1.68'), dbus.String(':1.46'), dbus.String(':1.49820'), dbus.String(':1.47'), dbus.String(':1.112'), dbus.String(':1.48'), dbus.String(':1.26'), dbus.String(':1.49'), dbus.String('org.xfce.PowerManager'), dbus.String(':1.27'), dbus.String(':1.114'), dbus.String(':1.28'), dbus.String('org.gtk.vfs.GPhoto2VolumeMonitor'), dbus.String('org.freedesktop.portal.Documents'), dbus.String('ca.desrt.dconf'), dbus.String(':1.54533'), dbus.String('org.kde.StatusNotifierItem-1337-1'), dbus.String('org.freedesktop.Tracker1'), dbus.String('org.freedesktop.ReserveDevice1.Audio0'), dbus.String(':1.54514'), dbus.String(':1.54437'), dbus.String(':1.54338'), dbus.String(':1.54515'), dbus.String(':1.54317'), dbus.String('org.manjaro.pamac.user'), dbus.String('org.freedesktop.FileManager1'), dbus.String('org.PulseAudio1'), dbus.String('org.gtk.vfs.mountpoint_http'), dbus.String('org.gnome.Nautilus'), dbus.String('org.gtk.vfs.GoaVolumeMonitor'), dbus.String('org.fcitx.Fcitx-1'), dbus.String('org.lxde.lxpolkit'), dbus.String('org.gnome.eog.ApplicationService'), dbus.String('com.canonical.Unity'), dbus.String(':1.91'), dbus.String('org.dockbar.DockbarX'), dbus.String('org.gnome.OnlineAccounts'), dbus.String(':1.71'), dbus.String('org.gnome.ScreenSaver'), dbus.String(':1.50'), dbus.String(':1.73'), dbus.String(':1.51'), dbus.String('org.gtk.vfs.Metadata'), dbus.String(':1.74'), dbus.String(':1.52'), dbus.String(':1.75'), dbus.String(':1.53'), dbus.String(':1.31'), dbus.String(':1.76'), dbus.String('org.freedesktop.portal.Fcitx'), dbus.String('org.freedesktop.impl.portal.PermissionStore'), dbus.String(':1.10'), dbus.String(':1.32'), dbus.String('org.gtk.vfs.mountpoint_dnssd'), dbus.String(':1.77'), dbus.String(':1.11'), dbus.String(':1.33'), dbus.String(':1.78'), dbus.String(':1.56'), dbus.String(':1.12'), dbus.String(':1.34'), dbus.String(':1.57'), dbus.String('com.github.wwmm.pulseeffects'), dbus.String(':1.0'), dbus.String(':1.13'), dbus.String(':1.35'), dbus.String(':1.58'), dbus.String(':1.1'), dbus.String(':1.14'), dbus.String('org.freedesktop.secrets'), dbus.String('org.gtk.vfs.mountpoint_2192'), dbus.String(':1.59'), dbus.String(':1.37'), dbus.String(':1.15'), dbus.String('org.gtk.vfs.MTPVolumeMonitor'), dbus.String(':1.38'), dbus.String(':1.3'), dbus.String(':1.16'), dbus.String(':1.39'), dbus.String(':1.17'), dbus.String(':1.104'), dbus.String('org.gtk.vfs.mountpoint_1381'), dbus.String(':1.18'), dbus.String(':1.54522'), dbus.String('org.gtk.vfs.mountpoint_4154'), dbus.String(':1.6'), dbus.String(':1.19')], signature=dbus.Signature('s'))

>>> dbus_intf.ListNames().mpris
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
AttributeError: 'dbus.Array' object has no attribute 'mpris'

>>> dbus_intf.ListNames()root_o = bus.get_object(name, '/org/mpris/MediaPlayer2')
KeyboardInterrupt

>>> root_o = bus.get_object('org.mpris.MediaPlayer2.spotify', '/org/mpris/MediaPlayer2')

>>> dbus.Interface(root_o, mpris + '.Player')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'mpris' is not defined

>>> 

>>> dbus.Interface(root_o, ''org.mpris.MediaPlayer2'' + '.Player')
  File "<stdin>", line 1
    dbus.Interface(root_o, ''org.mpris.MediaPlayer2'' + '.Player')
                               ^
SyntaxError: invalid syntax

>>> dbus.Interface(root_o, 'org.mpris.MediaPlayer2' + '.Player')
<Interface <ProxyObject wrapping <dbus._dbus.SessionBus (session) at 0x7f330ff31678> :1.88 /org/mpris/MediaPlayer2 at 0x7f330ff1e7f0> implementing 'org.mpris.MediaPlayer2.Player' at 0x7f330f2a4d30>

>>> def PropSet(prop, val):
...     global props
...     props.Set(mpris + '.Player', prop, val)
... 

>>> player      = dbus.Interface(root_o, mpris + '.Player')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'mpris' is not defined

>>> props = 'org.mpris.MediaPlayer2'

>>> V
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'V' is not defined

>>> player      = dbus.Interface(root_o, mpris + '.Player')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'mpris' is not defined

>>> mpris = 'org.mpris.MediaPlayer2'

>>> player      = dbus.Interface(root_o, mpris + '.Player')

>>> PropGet('PlaybackStatus')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
NameError: name 'PropGet' is not defined

>>> def PropGet(prop):
...     global props
...     return props.Get(mpris + '.Player', prop)
... 

>>>     global props
  File "<stdin>", line 1
    global props
    ^
IndentationError: unexpected indent

>>> PropGet('PlaybackStatus')
Traceback (most recent call last):
  File "<stdin>", line 1, in <module>
  File "<stdin>", line 3, in PropGet
AttributeError: 'str' object has no attribute 'Get'

>>> props       = dbus.Interface(root_o, dbus.PROPERTIES_IFACE)

>>> PropGet('PlaybackStatus')
dbus.String('Playing', variant_level=1)

>>> PropGet('Metadata')
dbus.Dictionary({dbus.String('mpris:trackid'): dbus.String('spotify:track:1AMzUKhF0vCFHZTx8H7OS4', variant_level=1), dbus.String('mpris:length'): dbus.UInt64(204187000, variant_level=1), dbus.String('mpris:artUrl'): dbus.String('https://open.spotify.com/image/8c4ac962a76d4f890b742b84211771a1b0adb1c7', variant_level=1), dbus.String('xesam:album'): dbus.String('エンドレスEP', variant_level=1), dbus.String('xesam:albumArtist'): dbus.Array([dbus.String('REOL')], signature=dbus.Signature('s'), variant_level=1), dbus.String('xesam:artist'): dbus.Array([dbus.String('REOL')], signature=dbus.Signature('s'), variant_level=1), dbus.String('xesam:autoRating'): dbus.Double(0.51, variant_level=1), dbus.String('xesam:discNumber'): dbus.Int32(1, variant_level=1), dbus.String('xesam:title'): dbus.String('LUVORATORRRRRY!', variant_level=1), dbus.String('xesam:trackNumber'): dbus.Int32(4, variant_level=1), dbus.String('xesam:url'): dbus.String('https://open.spotify.com/track/1AMzUKhF0vCFHZTx8H7OS4', variant_level=1)}, signature=dbus.Signature('sv'), variant_level=1)

>>> PropGet('Metadata')
dbus.Dictionary({dbus.String('mpris:trackid'): dbus.String('spotify:track:2f03A5QXiBmDUq7srhq1AX', variant_level=1), dbus.String('mpris:length'): dbus.UInt64(266370000, variant_level=1), dbus.String('mpris:artUrl'): dbus.String('https://open.spotify.com/image/0ac411484330c3e695321960bf95862d2753bf00', variant_level=1), dbus.String('xesam:album'): dbus.String('The First Waltz', variant_level=1), dbus.String('xesam:albumArtist'): dbus.Array([dbus.String('Omoi')], signature=dbus.Signature('s'), variant_level=1), dbus.String('xesam:artist'): dbus.Array([dbus.String('Omoi')], signature=dbus.Signature('s'), variant_level=1), dbus.String('xesam:autoRating'): dbus.Double(0.38, variant_level=1), dbus.String('xesam:discNumber'): dbus.Int32(1, variant_level=1), dbus.String('xesam:title'): dbus.String('Snow Drive (01.23)', variant_level=1), dbus.String('xesam:trackNumber'): dbus.Int32(2, variant_level=1), dbus.String('xesam:url'): dbus.String('https://open.spotify.com/track/2f03A5QXiBmDUq7srhq1AX', variant_level=1)}, signature=dbus.Signature('sv'), variant_level=1)
