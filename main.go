package main

import (
	"log"
	"os"
	"time"

	"./playerctl"
	"github.com/godbus/dbus"
	"github.com/gotk3/gotk3/glib"
	"github.com/gotk3/gotk3/gtk"
	"github.com/gotk3/gotk3/pango"
)

func initDbus() *dbus.Conn {
	conn, err := dbus.SessionBus()
	if err != nil {
		panic(err)
	}

	return conn
}

type Items struct {
	Labels   []*gtk.Label
	Notebook *gtk.Notebook
	Window   *gtk.Window
}

var (
	conn = initDbus()
)

func errC(err error) {
	if err != nil {
		panic(err)
	}
}

func csdVar() bool {
	csdbool := os.Getenv("GTK_CSD")
	switch csdbool {
	case "0":
		return false
	default:
		return true
	}
}

func refreshVar() time.Duration {
	refreshvar := os.Getenv("REFRESH")
	refreshint, e := time.ParseDuration(refreshvar + "ms")
	if e != nil && refreshvar == "" {
		return time.Millisecond * 200
	}

	log.Println("Reading env var REFRESH, setting to", refreshint)
	return refreshint
}

func newBoxText(content string) gtk.IWidget {
	box, err := gtk.BoxNew(gtk.ORIENTATION_VERTICAL, 0)
	errC(err)

	ent, err := gtk.EntryNew()
	errC(err)

	ent.SetText(content)
	ent.SetAlignment(0.5)

	box.PackStart(ent, true, true, 10)
	box.SetCenterWidget(ent)

	return box
}

func update() ([]playerctl.Player, string) {
	return playerctl.GetMetadata(), playerctl.GetStatus("")
}

var players = &playerctl.Players{}
var items = &Items{
	Labels: []*gtk.Label{},
}

func main() {
	err := players.Update(conn)
	if err != nil {
		panic(err)
	}

	log.Println(len(*players))

	// Initialize GTK without parsing any command line arguments.
	gtk.Init(nil)
	gtk.WindowSetDefaultIconName("audio-x-generic-symbolic")

	header, err := gtk.HeaderBarNew()
	if err != nil {
		log.Fatal("Could not create header bar:", err)
	}

	items.Window, err = gtk.WindowNew(gtk.WINDOW_TOPLEVEL)
	errC(err)

	header.SetShowCloseButton(csdVar())

	status := "Playing"
	header.SetTitle(status)

	playIcon, _ := gtk.ImageNewFromIconName("media-playback-start-symbolic", 0)
	pauseIcon, _ := gtk.ImageNewFromIconName("media-playback-pause-symbolic", 0)
	prevIcon, _ := gtk.ImageNewFromIconName("media-skip-backward-symbolic", 0)
	forwardIcon, _ := gtk.ImageNewFromIconName("media-skip-forward-symbolic", 0)

	buttonFn := []string{playerctl.Previous, playerctl.Next}

	imgArray := []*gtk.Image{playIcon, pauseIcon}

	ppButton := addppButton(imgArray, items.Notebook, header)

	buttonIcons := []*gtk.Image{
		prevIcon,
		forwardIcon,
	}

	for i := 0; i < len(buttonFn); i++ {
		button, err := gtk.ButtonNew()
		errC(err)

		button.SetImage(buttonIcons[i])

		action := buttonFn[i]

		button.Connect("clicked", func() {
			page, err := items.Notebook.GetNthPage(items.Notebook.GetCurrentPage())
			errC(err)

			label, err := items.Notebook.GetTabLabelText(page)
			errC(err)

			log.Println(action)

			playerctl.CtrlPlayer(label, action)
		}, items.Notebook)

		header.PackStart(button)
	}

	items.Notebook, err = gtk.NotebookNew()
	if err != nil {
		log.Fatalln(err)
	}

	items.Notebook.SetShowBorder(false)

	_ = items.reload()

	items.Notebook.Connect("switch-page", func() {
		items.refresh()
		glib.IdleAdd(func() {
			updateppButton(
				imgArray,
				getLabel(items.Notebook),
				ppButton,
				header,
			)
		})
	})

	items.Window.Add(items.Notebook)

	button, err := gtk.ButtonNewFromIconName("view-refresh-symbolic", 0)
	errC(err)

	_, err = button.Connect("clicked", func() {
		err := players.Update(conn)
		if err != nil {
			panic(err)
		}

		items.reload()
	})
	errC(err)

	header.PackStart(button)

	throttler := time.Tick(refreshVar())
	go func(imgArray []*gtk.Image, items *Items) {
		for {
			<-throttler

			if ppButton == nil || items == nil {
				continue
			}

			glib.IdleAdd(func() {
				updateppButton(
					imgArray,
					getLabel(items.Notebook),
					ppButton,
					header,
				)
				items.refresh()
			})
		}
	}(imgArray, items)

	items.Window.Connect("destroy", func() {
		gtk.MainQuit()
	})

	items.Window.SetPosition(gtk.WIN_POS_CENTER)

	items.Window.SetTitlebar(header)

	items.Window.SetDefaultSize(450, 150)
	items.Window.ShowAll()
	gtk.Main()
}

func (items *Items) reload() error {
	var err error
	items.Labels = []*gtk.Label{}

	for i := 0; i < items.Notebook.GetNPages(); i++ {
		items.Notebook.RemovePage(i)
	}

	err = players.Update(conn)
	if err != nil {
		log.Println(err)
		return err
	}

	if len(*players) == 0 {
		text, err := gtk.LabelNew("Not playing.")
		if err != nil {
			log.Println(err)
			return err
		}

		items.Window.Add(text)
		items.Labels = append(items.Labels, text)

		return nil

	}

	for _, player := range *players {
		content, _, trackID := playerctl.GiveMeSomething(player)

		text, err := gtk.LabelNew("")
		if err != nil {
			log.Println(err)
			continue
		}

		text.SetMarkup(content)
		text.SetTooltipText(trackID)
		//text.SetSelectable(true)
		text.SetLineWrapMode(pango.WRAP_WORD)
		text.SetLineWrap(true)

		label, err := gtk.LabelNew(player.Player)
		if err != nil {
			log.Println(err)
			continue
		}

		items.Notebook.AppendPage(text, label)
		items.Labels = append(items.Labels, text)
	}

	return nil
}

func (items *Items) refresh() {
	intP := items.Notebook.GetCurrentPage()
	if intP > -1 {
		page, _ := items.Notebook.GetNthPage(intP)
		labeltext, _ := items.Notebook.GetTabLabelText(page)

		player, e := playerctl.Populate(conn, "org.mpris.MediaPlayer2."+labeltext)
		if e != nil {
			log.Println(e)
			return
		}

		content, _, trackID := playerctl.GiveMeSomething(*player)

		ToolTip, e := items.Labels[intP].GetTooltipText()
		if e != nil {
			log.Println(e)
		}

		if trackID != ToolTip {
			items.Labels[intP].SetMarkup(content)
			items.Labels[intP].SetTooltipText(player.Metadata.MprisTrackid)
		}

	}
}

// play = [0], pause = [1]
func addppButton(icons []*gtk.Image, nb *gtk.Notebook, header *gtk.HeaderBar) *gtk.Button {
	button, err := gtk.ButtonNew()
	errC(err)

	button.SetImage(icons[1])

	button.Connect("clicked", func() {
		page, err := nb.GetNthPage(nb.GetCurrentPage())
		if err != nil {
			return
		}

		label, err := nb.GetTabLabelText(page)
		if err != nil {
			return
		}

		playerctl.CtrlPlayer(label, playerctl.PlayPause)

		updateppButton(icons, label, button, header)
	}, nb)

	header.PackStart(button)

	return button
}

func updateppButton(icons []*gtk.Image, playerID string, button *gtk.Button, header *gtk.HeaderBar) {
	if playerID == "" {
		return
	}

	status := playerctl.GetStatus(playerID)

	title := header.GetTitle()
	if title == status {
		return
	}

	header.SetTitle(status)

	if button == nil {
		return
	}

	switch status {
	case playerctl.Playing:
		button.SetImage(icons[1])
	default:
		button.SetImage(icons[0])
	}
}

func getLabel(nb *gtk.Notebook) string {
	page, e := nb.GetNthPage(nb.GetCurrentPage())
	if e != nil {
		return ""
	}

	label, e := nb.GetTabLabelText(page)
	if e != nil {
		return ""
	}

	return label
}

// func updatePlayers(labels []*gtk.Label, nb *gtk.Notebook) {
// 	players, _ := update()
// 	for i, label := range labels {
// 		nb.RemovePage(i)
// 		remove(players, i)
// 		for e, pl := range players {
// 			lbtxt, _ := label.GetText()
// 			if lbtxt == pl.Player {
// 				remove(players, e)
// 			}
// 		}
// 	}
// }

// func remove(s []playerctl.Player, i int) []playerctl.Player {
// 	s[i] = s[len(s)-1]
// 	return s[:len(s)-1]
// }
